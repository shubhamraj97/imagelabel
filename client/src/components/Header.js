import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Accounts from './Accounts';
import Landing from './Landing';
class Header extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     isAuthenticated: false
  //   };
  // }
  // componentDidMount() {
  //   if (this.props.currentUser) {
  //     console.log(this.props.currentUser);
  //     this.setState((isAuthenticated: true));
  //     console.log(this.props.currentUser);
  //   }
  // }
  // renderContent() {
  //   if (this.state.isAuthenticated) {
  //     return <h2>Welcome{this.props.currentUser}</h2>;
  //   }
  // }
  render() {
    const { currentUser } = this.props;
    if (currentUser) {
      return (
        <div>
          <nav className="navbar navbar-default">
            <div className="container-fluid">
              <div className="navbar-header">
                <a className="navbar-brand" href="/">
                  ImageLabel
                </a>
              </div>
              <ul className="nav navbar-nav">
                <li className="active">
                  <a href="/">Home</a>
                </li>
                <li>
                  <Accounts />
                </li>
              </ul>
            </div>
          </nav>
          <Landing />
        </div>
      );
    } else {
      return (
        <div>
          <nav className="navbar navbar-default">
            <div className="container-fluid">
              <div className="navbar-header">
                <a className="navbar-brand" href="/">
                  ImageLabel
                </a>
              </div>
              <ul className="nav navbar-nav">
                <li className="active">
                  <a href="/">Home</a>
                </li>
                <li>
                  <Accounts />
                </li>
              </ul>
            </div>
          </nav>
          <div className="jumbotron">
            <h1>Image Labeling Tool</h1>
            <p>
              ImageLabel is an interactive tool to manually collect useful data
              from images.
            </p>
          </div>
        </div>
      );
    }
  }
}

export default withTracker(props => {
  return {
    currentUser: Meteor.user()
  };
})(Header);
