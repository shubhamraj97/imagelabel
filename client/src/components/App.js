import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';

import Header from './Header';
class App extends Component {
  render() {
    return (
      <div>
        <Header />
      </div>
    );
  }
}

export default App;
