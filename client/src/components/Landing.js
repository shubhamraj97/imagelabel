import React, { Component } from 'react';
import axios from 'axios';
import FormList from './FormList';
var azure = require('azure-storage');
var parseString = require('xml2js').parseString;
var keys = require('../../config/keys');
var blobService = azure.createBlobService(
  keys.AzureStorageAccount,
  keys.AzurePassKey
);
class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = { images: [] };
  }
  async componentDidMount() {
    const result = [];
    const res = await axios.get(keys.BlobURL);
    parseString(res.data, async (err, value) => {
      result = value;
    });

    this.setState({
      images: result.EnumerationResults.Blobs[0].Blob
    });
  }
  renderContent() {
    if (this.state.images) {
      return <FormList images={this.state.images} />;
    }
  }
  render() {
    return (
      <div>
        <h2>Label The Image Below!!</h2>
        {this.renderContent()}
      </div>
    );
  }
}

export default Landing;
