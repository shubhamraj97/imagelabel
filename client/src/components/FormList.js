import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../../../imports/collections/images';
class FormList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      counter: 0,
      selectedAge: 'ageslotA',
      selectedGender: 'male'
    };
  }

  handleSubmit(event) {
    event.preventDefault();
    console.log(ReactDOM.findDOMNode(this.refs.sel1).value.trim());
    console.log(this.state.selectedAge);
    console.log(this.state.selectedGender);
    this.setState({ counter: this.state.counter + 1 });
  }

  handleAgeChange(changeEvent) {
    this.setState({
      selectedAge: changeEvent.target.value
    });
  }

  handleGenderChange(changeEvent) {
    this.setState({
      selectedGender: changeEvent.target.value
    });
  }

  renderContent() {
    if (
      this.props.images.length > 0 &&
      this.state.counter < this.props.images.length
    ) {
      return (
        <div>
          <img src={this.props.images[this.state.counter].Url} />
          <form onSubmit={this.handleSubmit.bind(this)}>
            <div style={{ margin: '5px 0' }}>
              <span style={{ margin: '0 10px' }}>Gender</span>
              <label className="radio-inline">
                <input
                  type="radio"
                  name="optradio"
                  value="male"
                  checked={this.state.selectedGender === 'male'}
                  onChange={this.handleGenderChange.bind(this)}
                />Male
              </label>
              <label className="radio-inline">
                <input
                  type="radio"
                  name="optradio"
                  value="female"
                  checked={this.state.selectedGender === 'female'}
                  onChange={this.handleGenderChange.bind(this)}
                />Female
              </label>
            </div>
            <div className="form-group">
              <label>Select Race:</label>
              <select className="form-control" ref="sel1" id="sel1">
                <option>American</option>
                <option>Asian</option>
                <option>Middle East</option>
                <option>Europen</option>
              </select>
            </div>
            <div>
              <strong>Select Age Range:</strong>
              <div className="radio" ref="age">
                <label>
                  <input
                    type="radio"
                    name="optradio2"
                    value="ageslotA"
                    checked={this.state.selectedAge === 'ageslotA'}
                    onChange={this.handleAgeChange.bind(this)}
                  />15-30
                </label>
              </div>
              <div className="radio">
                <label>
                  <input
                    type="radio"
                    name="optradio2"
                    value="ageslotB"
                    checked={this.state.selectedAge === 'ageslotB'}
                    onChange={this.handleAgeChange.bind(this)}
                  />30-60
                </label>
              </div>
              <div className="radio">
                <label>
                  <input
                    type="radio"
                    name="optradio2"
                    value="ageslotC"
                    checked={this.state.selectedAge === 'ageslotC'}
                    onChange={this.handleAgeChange.bind(this)}
                  />60 and Above
                </label>
              </div>
            </div>
            <button className="btn btn-primary">Submit</button>
          </form>
        </div>
      );
    } else if (
      this.props.images.length > 0 &&
      this.state.counter == this.props.images.length
    ) {
      return (
        <div className="jumbotron">
          <h1>Thank you for your response!</h1>
        </div>
      );
    }
  }
  render() {
    return <div>{this.renderContent()}</div>;
  }
}

export default FormList;
